from django.shortcuts import get_list_or_404, get_object_or_404, redirect, render
from django.http import Http404, HttpResponse
from .models import Student

# Create your views here.

def say_hello(request):
    default_name = 'user'
    name_ = request.GET.get('name', default_name)
    context = {'greeting': "Hi", 'name': name_}
    return render(request, 'hello/greetings.html', context)


def say_goodbye(request):
    context = {'greeting':"Goodbye world!"}
    return render(request, 'hello/greetings.html', context)

def nth_fibonacci(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return nth_fibonacci(n - 1) + nth_fibonacci(n - 2)

def get_nth_fibonacci(request):
    n = int(request.GET.get('n', 0))
    fibonacci = nth_fibonacci(n)
    context = {'n': n,'fibonacci':fibonacci}
    return render(request, 'hello/fibonacci.html', context)

def get_nth_fibonacci_alt(request, n):
    fibonacci = nth_fibonacci(n)
    context = {'n': n,'fibonacci':fibonacci}
    return render(request, 'hello/fibonacci.html', context)

def nth_fibonacci_post(request):
    if request.method == 'GET':
        return render(request, 'hello/fibonacci.html')
    if request.method == 'POST':
        n = int(request.POST.get('num', 0))
        fibonacci = nth_fibonacci(n)
        context = {'n': n, 'fibonacci': fibonacci}
        return render(request, 'hello/fibonacci.html', context)
    
def student_list(request):
    students = get_list_or_404(Student)
    context = {'students': students}
    return render(request, 'hello/student_list.html', context)

def student_detail(request, roll_no_):
    student = get_object_or_404(Student, roll_no=roll_no_)

    # try:
    #     student = Student.objects.get(roll_no=roll_no_)
    # except Student.DoesNotExist:
    #     raise Http404(f'student with roll number {roll_no_} does not exist')
    context = {'student': student}
    return render(request, 'hello/student_detail.html', context)

def update_or_create_student(request, id_=None):
    if request.method == "GET":
        context = {}
        if id_ is not None:
            s = get_object_or_404(Student, pk=id_)
            context['student'] = s
        return render(request, 'hello/admission_form.html', context)
    if request.method == "POST":
        name_ = request.POST.get('name', '')
        roll_no_ = int(request.POST.get('roll', '0'))
        id = int(request.POST.get('id', '0'))
        if id != 0:
            s = get_object_or_404(Student, id=id)
            s.name = name_
            s.roll_no = roll_no_
        else:
            s = Student.objects.create(roll_no = roll_no_, name=name_)
        s.save()
        return redirect("student_list")

def delete_student(request, roll_no_):
    s = get_object_or_404(Student, roll_no=roll_no_)
    s.delete()
    return redirect("student_list")



